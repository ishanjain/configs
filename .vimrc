
" Set <SPACE> as <Leader>
let mapleader = " "

" call pathogen#infect()

" Enable filetype plugins
filetype plugin on

" Search files in nested directories
" set path+=**
" set clipboard=unnamedplus

" Make colors work in TMUX session
" set term=screen-256color

syntax enable
set number
set relativenumber
set ts=4
set autoindent
set expandtab
set shiftwidth=4
set cursorline
set showmatch
set mouse=a
set encoding=utf-8
set textwidth=150

" Fixing stupid defaults
set splitbelow
set splitright

" Autocompletion
set wildmode=longest,list,full
set wildmenu

" Color Scheme
let g:solarized_termcolors=256
set background=dark
colorscheme gruvbox

" Key Mappings
" Plugins
" map <C-n> :NERDTreeToggle<CR>

" nnoremap ; :
" nnoremap <Leader> :nohlsearch<CR>
" " Escape
" inoremap ;[ <esc>
" nnoremap <leader>s :mksession<CR>
" " nnoremap <C-Left> :tabprevious<CR>
" " nnoremap <C-Right> :tabnext<CR>
"
" " Fix for Arrow keys in Alacritty
" map <ESC>^[[1;5D <C-Left>
" map <ESC>^[[1;5C <C-Right>
" map! <ESC>^[[1:5D <C-Left>
" map! <ESC>^[[1;5C <C-Right>
"
" " Splitting shortcuts
" nnoremap <silent> <Leader>s :split<CR>
" nnoremap <silent> <Leader>v :vsplit<CR>
" nnoremap <silent> <Leader>q :close<CR>
"
" " Copy/Paste
" vmap <silent> <C-y> "+y
" vmap <silent> <C-p> "+p
"
" call plug#begin()
" " nvim plugins
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" call plug#end()
"
