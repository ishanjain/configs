# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/home/ishan/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="cypher"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=( rust )

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#
fpath+=~/.zfunc

#Golang
export PATH="/opt/renderdoc_1.0/bin:/usr/local/go/bin:/home/ishan/go/bin:/usr/bin:$HOME/.cargo/bin:$PATH:/opt/android-sdk/build-tools/28.0.3/"
export GOPATH="/home/ishan/go"
export NODE_PATH="/usr/lib/nodejs:/usr/lib/node_modules:/usr/share/javascript"
#
#Update xterm to use ~/.Xresources
#xrdb -merge ~/.Xresources
#alias qemu=qemu-system-x86_64
alias vi=vim
alias ls=exa
# Start tmux when xterm starts
#[[ $- != *i* ]] && return
#[[ -z "$TMUX" ]] && exec tmux new-session -A

# Alias xdg-open to open
alias open=xdg-open
export EDITOR=vim
alias qemu=qemu-system-x86_64
alias android-studio=/home/ishan/.android-studio/bin/studio.sh
# source this file into an existing shell.

export VULKAN_SDK="/opt/VulkanSDK/1.1.73.0/x86_64"
export PATH="$VULKAN_SDK/bin:$PATH:/opt/android-sdk/build-tools/28.0.2"
export LD_LIBRARY_PATH="$VULKAN_SDK/lib:${LD_LIBRARY_PATH:-}"
export VK_LAYER_PATH="$VULKAN_SDK/etc/explicit_layer.d:/opt/renderdoc_1.0/etc/vulkan/implicit_layer.d"
alias vim=nvim
alias speedtest="speedtest --bytes"
alias kindle="WINEPREFIX="/home/ishan/.wine" wine C:\\\\Program\\ Files\\ \\(x86\\)\\\\Amazon\\\\Kindle\\\\Kindle.exe"

# Set Firefox Work Profile Alias
alias firefoxwork="firefox -P Work"
alias tfirefox="firefox -no-remote -new-instance -profile $(mktemp -d -p /tmp)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Load git aliases
source ~/.git_aliases


